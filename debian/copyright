Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xcolorsel
Source: ftp://ftp.x.org/R5contrib

Files: 00-ANNOUNCE
       00-README
       01-CHANGELOG
       01-COPYING
       01-INSTALL
       app2head
       appdef-c.h
       appdef.h
       biglens
       biglensMask
       config.h
       debug-R6.generic.makefile
       defhelp.h
       Imakefile
       lens
       lensMask
       R5.generic.makefile
       R6.generic.makefile
       RgbSink.c
       RgbSink.h
       RgbSinkP.h
       RgbSrc.c
       RgbSrc.h
       RgbSrcP.h
       RgbText.c
       RgbText.h
       RgbTextP.h
       rgb.txt
       xcolorsel-1.1a.lsm
       Xcolorsel.ad
       xcolorsel.c
       Xcolorsel-color.ad
       Xcolorsel.help
       xcolorsel.man
       xcslicon
Copyright: 1987-1988, Massachusetts Institute of Technology
           1993-1994, Michael Weller
License: GPL-2+ and MIT-variant

Files: debian/*
Copyright: 1996,      Larry Daffner <vizzie@airmail.net>
           1997-1999, Mark Baker <mbaker@iee.org>
           2001,      Branden Robinson <branden@debian.org>
           2001,      Carlos Laviola <claviola@ajato.com.br>
           2001,      Colin Watson <cjwatson@debian.org>
           2002,      Ian Zimmerman <itz@speakeasy.org>
           2003-2008, Decklin Foster <decklin@red-bean.com>
           2012,      Frank Lichtenheld <djpig@debian.org>
           2015,      Santiago Vila <sanvila@debian.org>
           2016-2023, Paulo Henrique de Lima Santana (phls) <phls@debian.org>
License: MIT-variant

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: MIT-variant
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the name of M.I.T. not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission. M.I.T. makes no representations about the
 suitability of this software for any purpose. It is provided "as is"
 without express or implied warranty.
 .
 M.I.T. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL M.I.T.
 BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
